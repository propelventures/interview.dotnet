## Requirements

* Your favourite IDE (Rider, VS Code or Visual Studio)
* dotnet core sdk installed

## Tasks - Overview

In the interview we will ask you to walk through your solution for one of the tasks mentioned below. We can provide a MacBook with Rider or VS Code to work through in front of us or feel free to bring your solution in using your own dev environment. We generally use a OSX/linux environment.

### Part 1: Implement a remove items from basket method

  1. Extend that application to remove items from the basket.
  2. Write a test case to cover the cases where you would remove an item from the basket.
